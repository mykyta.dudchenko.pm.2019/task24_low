﻿using System;
using System.Text;

namespace Task_23.Models
{
    public class ArticleModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }
    }
}