﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task_23.Models
{
    public class QuizAnswerModel
    {
        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Surname is required")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Choose your framework")]
        public string framework { get; set; }
        [Required(ErrorMessage = "Choose your language")]
        public string language { get; set; }
        public Dictionary<string,bool> fruits { get; set; }

    }
}