﻿using System;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Task_23.Models
{
    public class FeedbackModel
    {
        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string AuthorName { get; set; }
        [Required(ErrorMessage = "Surname is required")]
        [Display(Name = "Surname")]
        public string AuthorSurname { get; set; }
        [Required]
        public DateTime FeedbackDate { get; set; }
        [Required(ErrorMessage = "Text is required")]
        [Display(Name = "Your opinion")]
        public string FeedbackText { get; set; }
    }
}