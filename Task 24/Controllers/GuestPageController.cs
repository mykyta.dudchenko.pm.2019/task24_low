﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task_23.Models;

namespace Task_23.Controllers
{
    public class GuestPageController : Controller
    {
        static List<FeedbackModel> feedbacks = new List<FeedbackModel>()
        {
            new FeedbackModel {AuthorName = "Vasyl", AuthorSurname = "M.", FeedbackDate = new DateTime(2021,4,23), FeedbackText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at urna eget dui tempus iaculis. Morbi cursus lacus non metus malesuada, quis aliquet quam auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean fringilla ultricies velit congue egestas. Nullam interdum scelerisque nulla. Integer tempus sit amet sem vel vehicula. Maecenas ac facilisis nulla."},
            new FeedbackModel {AuthorName = "Nazar", AuthorSurname = "D.", FeedbackDate = new DateTime(2021,9,13), FeedbackText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at urna eget dui tempus iaculis. Morbi cursus lacus non metus malesuada, quis aliquet quam auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean fringilla ultricies velit congue egestas. Nullam interdum scelerisque nulla. Integer tempus sit amet sem vel vehicula. Maecenas ac facilisis nulla."},
            new FeedbackModel {AuthorName = "Rostyslav", AuthorSurname = "V.", FeedbackDate = new DateTime(2021,5,12), FeedbackText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at urna eget dui tempus iaculis. Morbi cursus lacus non metus malesuada, quis aliquet quam auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean fringilla ultricies velit congue egestas. Nullam interdum scelerisque nulla. Integer tempus sit amet sem vel vehicula. Maecenas ac facilisis nulla."},
            new FeedbackModel {AuthorName = "Egor", AuthorSurname = "S.", FeedbackDate = new DateTime(2021,2,11), FeedbackText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at urna eget dui tempus iaculis. Morbi cursus lacus non metus malesuada, quis aliquet quam auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean fringilla ultricies velit congue egestas. Nullam interdum scelerisque nulla. Integer tempus sit amet sem vel vehicula. Maecenas ac facilisis nulla."},
            new FeedbackModel {AuthorName = "Ostap", AuthorSurname = "K.", FeedbackDate = new DateTime(2021,10,23), FeedbackText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at urna eget dui tempus iaculis. Morbi cursus lacus non metus malesuada, quis aliquet quam auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean fringilla ultricies velit congue egestas. Nullam interdum scelerisque nulla. Integer tempus sit amet sem vel vehicula. Maecenas ac facilisis nulla."},
            new FeedbackModel {AuthorName = "Olya", AuthorSurname = "Y.", FeedbackDate = new DateTime(2021,11,15), FeedbackText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at urna eget dui tempus iaculis. Morbi cursus lacus non metus malesuada, quis aliquet quam auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean fringilla ultricies velit congue egestas. Nullam interdum scelerisque nulla. Integer tempus sit amet sem vel vehicula. Maecenas ac facilisis nulla."}
        };
        public ActionResult GuestPage()
        {
            ViewBag.feed = feedbacks.OrderBy(x=>x.FeedbackDate);
            return View();
        }
        [HttpPost]
        public ActionResult GuestPage(FeedbackModel feedback)
        {
            feedback.FeedbackDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                feedbacks.Add(feedback);
                return RedirectToAction("GuestPage");
            }
            return GuestPage();
        }
    }
}