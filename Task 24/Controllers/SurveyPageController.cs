﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task_23.Models;

namespace Task_23.Controllers
{
    public class SurveyPageController : Controller
    {
        QuizAnswerModel res = new QuizAnswerModel();

        [HttpGet]
        public ActionResult Index()
        {
            res.fruits = new Dictionary<string, bool>() { { "Banana", false },{ "Apple", false }, { "Orange", false }, { "Melon", false }, { "Watermelon", false } };
            return View(res);
        }

        [HttpPost]
        public ActionResult Index(QuizAnswerModel answers)
        {
            if(ModelState.IsValid)
            {
                return View("Submitted", answers);
            }
            return Index();
        }
    }
}